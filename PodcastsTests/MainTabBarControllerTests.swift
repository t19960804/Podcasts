//
//  NetworkServiceTests.swift
//  PodcastsTests
//
//  Created by t19960804 on 1/3/21.
//  Copyright © 2021 t19960804. All rights reserved.
//

import XCTest
import Combine
import FeedKit

@testable import Podcasts

class MainTabBarControllerTests: XCTestCase {
    var subscribers: Set<AnyCancellable>!
    var viewModel: MainTabBarViewModel!
    var mockURLSession = MockURLSession()
    
    override func setUp() {
        super.setUp()
        subscribers = Set<AnyCancellable>()
        viewModel = MainTabBarViewModel()
        NetworkService.sharedInstance.replaceSession(with: mockURLSession)
    }
    override func tearDown() {
        super.tearDown()
        subscribers.removeAll()
        viewModel = nil
    }
    
    func testGetSubscriptionStatus_Ongoing(){
        let path = Bundle.main.path(forResource: "SubscriptionData_Ongoing", ofType: "json")!
        mockURLSession.setTestDataPath(path: path)
        guard let request = viewModel.createValidationRequest() else {
            print("ValidationRequest is nil, no need to test")
            return
        }
        let publisher = NetworkService.sharedInstance.getSubscriptionStatus(request: request, productID: productID_OneMonth)
        publisher
            .sink { _ in
                
            } receiveValue: { (status) in
                print(status)
                XCTAssert(status == "0", "status應該要為0,代表還在訂閱中")
            }.store(in: &subscribers)
    }
    
    func testGetSubscriptionStatus_Cancel(){
        let path = Bundle.main.path(forResource: "SubscriptionData_Cancel", ofType: "json")!
        mockURLSession.setTestDataPath(path: path)
        guard let request = viewModel.createValidationRequest() else {
            print("ValidationRequest is nil, no need to test")
            return
        }
        let publisher = NetworkService.sharedInstance.getSubscriptionStatus(request: request, productID: productID_OneMonth)
        publisher
            .sink { _ in
                
            } receiveValue: { (status) in
                print(status)
                XCTAssert(status == "1", "status應該要為1,代表訂閱過期")
            }.store(in: &subscribers)
    }
}

