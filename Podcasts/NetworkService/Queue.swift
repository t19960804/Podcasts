//
//  PodcastsDownloadQueue.swift
//  Podcasts
//
//  Created by t19960804 on 4/26/21.
//  Copyright © 2021 t19960804. All rights reserved.
//

import Foundation

class Queue<T> {
    private var array = [T]()
    func enqueue(item: T){
        array.append(item)
    }
    func dequeue() -> T? {
        return array.removeFirst()
    }
    var isEmpty: Bool {
        return array.isEmpty
    }
    var count: Int {
        return array.count
    }
}
