//
//  KeychainManager.swift
//  Podcasts
//
//  Created by t19960804 on 4/2/21.
//  Copyright © 2021 t19960804. All rights reserved.
//

import Foundation
import Security

class KeychainManager {
    static let shared = KeychainManager()
    let testKey = "TestKey"
    let subscriptionKey = "hasSubscription"
    let receiptDataKey = "receiptData"
    private init() {}
    //OSStatus > The status of the operation we just performed
    //If status is 0 (or errSecSuccess), the operation finished successfully
    
    //kSecClass: A key whose value is the item's class.
    //kSecClassGenericPassword: The value that indicates a generic password item.
    //kSecAttrAccount: A key whose value is a string indicating the item's account name.
    //kSecValueData: A key whose value is the item's data.
    func save(key: String, data: Data){
        let query = [
            kSecClass        : kSecClassGenericPassword,
            kSecAttrAccount  : key,
            kSecValueData    : data ] as CFDictionary

        _ = SecItemDelete(query)
        //print("Info - Delete from keychain: \(statusOfDelete)")
        _ = SecItemAdd(query , nil)
        //print("Info - Save to keychain: \(statusOfAdd)")
    }
    //kSecReturnData: When this is set to true, this will return the actual sensitive data stored in the keychain item.
    //kSecMatchLimit: If you specify a value bigger than 1, you will get a CFArray of CFDictionary.
    func fetch(key: String) -> Data? {
        let query = [
            kSecClass        : kSecClassGenericPassword,
            kSecAttrAccount  : key,
            kSecReturnData   : true,
            kSecMatchLimit   : 1 ] as CFDictionary

        var dataTypeRef: AnyObject?

        _ = SecItemCopyMatching(query, &dataTypeRef)
        //print("Info - Fetch from keychain: \(status)")
        return dataTypeRef as? Data? ?? nil
    }
    
    func update(key: String, data: Data) {
        let query = [
          kSecClass: kSecClassGenericPassword,
          kSecAttrAccount: key
        ] as CFDictionary

        let updateFields = [
          kSecValueData: data
        ] as CFDictionary
        _ = SecItemUpdate(query, updateFields)
        //print("Info - Update to keychain: \(status)")
    }
}
