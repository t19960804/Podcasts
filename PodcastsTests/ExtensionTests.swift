//
//  ExtensionTests.swift
//  PodcastsTests
//
//  Created by t19960804 on 4/16/21.
//  Copyright © 2021 t19960804. All rights reserved.
//

import XCTest
@testable import Podcasts

class ExtensionTests: XCTestCase {

    override func setUp() {
        super.setUp()
       
    }
    override func tearDown() {
        super.tearDown()
        
    }
    func testExtensionMainTabbarController(){
        let favoritesController = UIApplication.mainTabBarController?.favoritesController
        XCTAssertNotNil(favoritesController)
        let downloadController = UIApplication.mainTabBarController?.downloadController
        XCTAssertNotNil(downloadController)
    }
    func testExtensionString(){
        let string = "https://www.youtube.com/"
        let httpsString1 = string.turnHTTPToHTTPS()
        XCTAssertTrue(httpsString1 == "https://www.youtube.com/")
        
        let httpString = "http://www.youtube.com/"
        let httpsString2 = httpString.turnHTTPToHTTPS()
        XCTAssertTrue(httpsString2 == "https://www.youtube.com/")
    }
    func testExtensionUIImage(){
        let images = UIImage.audioImages
        XCTAssertTrue(images.count == 4)
    }
}
