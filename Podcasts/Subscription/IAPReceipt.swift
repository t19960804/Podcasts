//
//  IAPReceipt.swift
//  Podcasts
//
//  Created by t19960804 on 4/3/21.
//  Copyright © 2021 t19960804. All rights reserved.
//

import Foundation

struct IAPReceipt: Decodable {
    let environment: String
    let pending_renewal_info: [PendingRenewalInfo]
}
struct PendingRenewalInfo: Decodable {
    let product_id: String
    let auto_renew_status: String//Can not be interpreted as the customer’s subscription status.
    var expiration_intent: String?//Only present for a receipt containing an expired auto-renewable subscription.
}
