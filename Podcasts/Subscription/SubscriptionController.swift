//
//  SubscriptionController.swift
//  Podcasts
//
//  Created by t19960804 on 3/28/21.
//  Copyright © 2021 t19960804. All rights reserved.
//

import UIKit
import LBTATools
import StoreKit
import JGProgressHUD

let productID_OneMonth = "FreeDownload_0804"

class SubscriptionController: UIViewController {
    private var purchasedByManaul = false
    
    private let indeterminateHud: JGProgressHUD = {
        let hud = JGProgressHUD()
        let view = JGProgressHUDIndeterminateIndicatorView()
        hud.indicatorView = view
        view.setColor(.purple)
        hud.textLabel.textColor = .purple
        hud.textLabel.text = "Purchasing..."
        return hud
    }()
    private let errorHud: JGProgressHUD = {
        let hud = JGProgressHUD()
        let view = JGProgressHUDErrorIndicatorView()
        hud.indicatorView = view
        view.tintColor = .purple
        hud.textLabel.textColor = .purple
        return hud
    }()
    private let titleLabel: UILabel = {
        let lb = UILabel()
        lb.text = "Podcast"
        lb.font = .boldSystemFont(ofSize: 40)
        lb.textColor = .purple
        return lb
    }()
    private let descrptionLabel: UILabel = {
        let lb = UILabel()
        lb.text = "For just $10 a month, you can download the podcast you want whenever, Treat yourself with this all access pass to a world of wonderful podcasts"
        lb.numberOfLines = 0
        lb.font = .systemFont(ofSize: 24)
        lb.textColor = .gray
        return lb
    }()
    private let subscribeBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("$10 / Month", for: .normal)
        btn.backgroundColor = .purple
        btn.layer.cornerRadius = 5
        btn.clipsToBounds = true
        btn.tintColor = .white
        btn.titleLabel?.font = .boldSystemFont(ofSize: 20)
        btn.addTarget(self, action: #selector(handleSubscribeOneMonth), for: .touchUpInside)
        return btn
    }()
    private let freeTrialBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Free Trial", for: .normal)
        btn.layer.cornerRadius = 5
        btn.clipsToBounds = true
        btn.tintColor = .purple
        btn.titleLabel?.font = .boldSystemFont(ofSize: 20)
        btn.backgroundColor = .clear
        btn.layer.borderWidth = 2
        btn.layer.borderColor = UIColor.purple.cgColor
        btn.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        return btn
    }()
    private let spaceView: UIView = {
        let v = UIView()
        return v
    }()
    private lazy var vStackView: UIStackView = {
        let sv = UIStackView(arrangedSubviews: [titleLabel,descrptionLabel,spaceView,subscribeBtn,freeTrialBtn])
        sv.axis = .vertical
        sv.alignment = .center
        //.fillEqually > 不能修改元件的size,App會平均分配每個元件的size去填滿stack view
        //.fillProportionally > 可以修改元件的size,但是元件們最後還是會填滿stack view
        sv.distribution = .fillProportionally
        sv.spacing = 8
        return sv
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        SKPaymentQueue.default().add(self)
        
        let data = "false".data(using: String.Encoding.utf8)!
        KeychainManager.shared.save(key: KeychainManager.shared.subscriptionKey, data: data)
        
        view.backgroundColor = .white
        setupConstraints()
    }
    fileprivate func setupConstraints(){
        view.addSubview(vStackView)
        vStackView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 20, left: 20, bottom: 20, right: 20))
        spaceView.anchor(top: nil, leading: vStackView.leadingAnchor, bottom: nil, trailing: vStackView.trailingAnchor, size: .init(width: 0, height: 90))
        subscribeBtn.anchor(top: nil, leading: vStackView.leadingAnchor, bottom: nil, trailing: vStackView.trailingAnchor, size: .init(width: 0, height: 60))
        freeTrialBtn.anchor(top: nil, leading: vStackView.leadingAnchor, bottom: nil, trailing: vStackView.trailingAnchor, size: .init(width: 0, height: 60))
    }
    @objc fileprivate func handleSubscribeOneMonth(){
        //SKPaymentQueue > 儲存付款資訊的Queue,可儲存多筆
        if SKPaymentQueue.canMakePayments() {
            let paymentRequest = SKMutablePayment()
            paymentRequest.productIdentifier = productID_OneMonth
            //每加入一個SKPayment物件就會產生一個SKPaymentTransaction物件來處理payment並加入到SKPaymentQueue,然後AppStore會處理這個Queue
            SKPaymentQueue.default().add(paymentRequest)
        } else {
            print("Err - 使用者無法支付")
        }
    }
    @objc fileprivate func handleDismiss(){
        self.dismiss(animated: true) {
            
        }
    }
}
extension SubscriptionController: SKPaymentTransactionObserver {
    //付款進度
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        transactions.forEach {
            switch $0.transactionState {
            case .purchased:
                let productID = $0.payment.productIdentifier
                if productID == productID_OneMonth {
                    print("Info - 訂閱一個月成功")
                    let appStoreReceiptURL = Bundle.main.appStoreReceiptURL
                    if FileManager.default.fileExists(atPath: appStoreReceiptURL!.path) {
                        let receiptData = try! Data(contentsOf: appStoreReceiptURL!, options: .alwaysMapped)
                        KeychainManager.shared.save(key: KeychainManager.shared.receiptDataKey, data: receiptData)
                        print("Info - Save receipt data success")
                    } else {
                        print("Err - File is not exist in appStoreReceiptURL")
                    }
                    //若是訂閱已經過期,使用者會進來這頁面,但是.updatedTransactions會因為之前的續訂而觸發
                    //所以要防止訂閱頁面自動ismiss,應該要透過訂閱按鈕觸發訂閱流程然後dismiss
                    if purchasedByManaul {
                        self.dismiss(animated: true) {
                            let data = "true".data(using: String.Encoding.utf8)!
                            KeychainManager.shared.save(key: KeychainManager.shared.subscriptionKey, data: data)
                        }
                    }
                } else {
                    print("Warn - 購買到非預期的商品:\(productID)")
                }
                queue.finishTransaction($0)
                self.indeterminateHud.dismiss()
            case .failed:
                if let err = $0.error {
                    print("Err - 購買失敗:\(err.localizedDescription)")
                    errorHud.textLabel.text = "\(err.localizedDescription), Please try again"
                    errorHud.show(in: view)
                    errorHud.dismiss(afterDelay: 2.5, animated: true)
                }
                queue.finishTransaction($0)
                indeterminateHud.dismiss()
            case .restored:
                print("Info - 恢復之前購買")
                queue.finishTransaction($0)
            case .purchasing:
                print("Info - 購買中")
                purchasedByManaul = true
                indeterminateHud.show(in: view)
            default:
                print("Warn - 未知的transactionState:\($0.transactionState)")
            }
        }
    }
}
