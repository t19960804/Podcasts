//
//  MainTabBarViewModel.swift
//  Podcasts
//
//  Created by t19960804 on 4/11/21.
//  Copyright © 2021 t19960804. All rights reserved.
//

import Foundation
import Combine

class MainTabBarViewModel {
    private let sharedSecretKey = "02e2fe0e04754743896bee390dcdf79d"
    private var fetchDataSubscriber: AnyCancellable?
    
    @Published var isUnsubscribing = true
    @Published var isValidationComplete = false
    
    func validatingReceiptsWithTheAppStore(productID: String) {
        guard let request = createValidationRequest() else { return }
        isValidationComplete = false
        let publisher = NetworkService.sharedInstance.getSubscriptionStatus(request: request, productID: productID)
        fetchDataSubscriber = publisher
            .receive(on: DispatchQueue.main)
            .sink { (completion) in
                switch completion {
                case .failure(let error):
                    print("Err - 驗證收據失敗:\(error.localizedDescription)")
                    self.isUnsubscribing = true
                case .finished:
                    print("Info - 驗證完成")
                }
                self.isValidationComplete = true
            } receiveValue: { (status) in
                switch status {
                case "0":
                    print("Info - 使用者還在訂閱中")
                    self.isUnsubscribing = false
                case "1":
                    print("Info - 使用者主動取消訂閱或訂閱過期")
                    self.isUnsubscribing = true
                default:
                    print("Err - 未知的訂閱狀態:\(status)")
                }
            }
    }
    func createValidationRequest() -> URLRequest? {
        //有兩種確認訂閱狀態方式
        //1.主動發request去確認
        //2.在AppStoreConnect填寫"伺服器通知URL",AppStore Server會在訂閱的狀態改變時去呼叫那個URL
        let appStoreReceiptURL = Bundle.main.appStoreReceiptURL!
        let isReceiptFileExist = FileManager.default.fileExists(atPath: appStoreReceiptURL.path)
        var data: Data?
        if isReceiptFileExist {
            print("Info - 使用現存的ReceiptFile")
            data = try! Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
        } else {
            print("Info - 使用備份的ReceiptFile")
            data = KeychainManager.shared.fetch(key: KeychainManager.shared.receiptDataKey)
        }
        guard let receiptData = data else {
            print("Err - receiptData is nil")
            return nil
        }
        let receiptString = receiptData.base64EncodedString(options: [])
        let parameters = ["receipt-data":receiptString, "password": sharedSecretKey]
        let environment = appStoreReceiptURL.lastPathComponent == "sandboxReceipt" ? "sandbox" : "buy"
        let url = URL(string: "https://\(environment).itunes.apple.com/verifyReceipt")!
        let request = URLRequest(url: url, parameters: parameters, method: .POST)
        return request
    }

}
